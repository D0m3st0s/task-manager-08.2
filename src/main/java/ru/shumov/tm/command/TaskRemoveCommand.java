package ru.shumov.tm.command;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.enums.Role;

public class TaskRemoveCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task remove";
    @Getter
    private final String description = "task remove: Выборочное удаление задач.";

    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_ID_FOR_REMOVING);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            @Nullable final var task = bootstrap.getTaskService().getOne(taskId);
            if (task == null) {
                bootstrap.getTerminalService().outPutString(Constants.TASK_DOES_NOT_EXIST);
                return;
            }
            if (task.getUserId().equals(user.getId())) {
                bootstrap.getTaskService().remove(taskId);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskRemoveCommand() {
    }
}
