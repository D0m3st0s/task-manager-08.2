package ru.shumov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;

import java.util.Comparator;

public class ProjectComparatorServiceImpl implements ProjectComparatorService, Comparator<Project> {

    private String method;

    public ProjectComparatorServiceImpl(String method) {
        this.method = method;
    }

    public int compare(@NotNull final Project pr1, @NotNull final Project pr2) {
        switch (method) {
            case "by creating date":
                return pr1.getCreatingDate().compareTo(pr2.getCreatingDate());
            case "by start date":
                return pr1.getStartDate().compareTo(pr2.getStartDate());
            case "by end date":
                return pr1.getEndDate().compareTo(pr2.getEndDate());
            case "by status":
                return Integer.compare(pr1.getStatus().getNumber(), pr2.getStatus().getNumber());
            default:
                return 0;
        }
    }
}
